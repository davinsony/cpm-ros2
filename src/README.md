# Creación de paquetes Python en ROS

Para crear un paquete usamos la siguiente linea de comandos

```bash
ros2 pkg create ev3 --build-type ament_python --license GPL-3.0-only --maintainer-name "Davinson Castaño" --maintainer-email "cpm@davinsony.com"
```
Para que efectivamente funcione el paquete necesitamos configurar dos archivos:

- package.xml

    Aquí agregamos las dependencias de nuestro paquete, en general: 

    ```xml
    <exec_depend>rclpy</exec_depend>
    <exec_depend>std_msgs</exec_depend>
    ```

- setup.py

    Aquí agregamos los ejectuables del paquete, ejemplo: 

    ```python
        entry_points={
            'console_scripts': [
                'diferential = ev3.diferential_robot:main',
            ],
        },    
    ```
