# Plataformas Moviles

Para usar este paquete necesitamos (estando en la carpeta raiz `cpm-ros2`):

1. Construirlo usando `colcon build --packages-select plataformas_moviles`
2. Declarando el path usando `source install\setup.bash`
3. Correr el robot `ros2 run plataformas_moviles uniciclo`
4. Correr en otro terminal `ros2 run rviz2 rviz2` y abrir la configuración `.rviz` que está en el paquete. 

Para los siguientes ejemplos correremos uno a la vez en un tercer terminal:

- Intento #1 `ros2 run plataformas_moviles controlador`
- Intento #2 `ros2 run plataformas_moviles controlador --ros-args -p correction:=True`
- Intento #3 `ros2 run plataformas_moviles controlador --ros-args -p correction:=True -p gain:=5`