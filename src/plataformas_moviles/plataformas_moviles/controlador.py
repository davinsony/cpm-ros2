# Copyright 2023 Davinson Castano Cano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np

import rclpy
from rclpy.node import Node

from geometry_msgs.msg import Twist, PoseStamped
from tf_transformations import euler_from_quaternion

class Controlador(Node):

    def __init__(self):
        super().__init__('controlador')
        
        self.xd = 0.0
        self.yd = 0.0

        self.x = 0.0
        self.y = 0.0
        self.phi = 0.0
        
        self.declare_parameter('dt',1/20)    # seconds
        self.dt = self.get_parameter('dt').value

        self.declare_parameter('v0',0.5)    # m/s
        self.cmd_vel = Twist()
        self.cmd_vel.linear.x = self.get_parameter('v0').value

        self.declare_parameter('gain',0.5)    # m/s
        self.gain = self.get_parameter('gain').value

        self.declare_parameter('correction',False)    # m/s
        self.correction = self.get_parameter('correction').value

        # TOPIC PUBLISHERS

        self.publisher_cmd_vel = self.create_publisher(Twist, '/cmd_vel', 1)
        self.publisher_goal_pose = self.create_publisher(PoseStamped, '/goal_pose', 1)

        # TOPIC SUBSCRIBERS

        self.sensor_input_sub = self.create_subscription(PoseStamped,'/pose',self.sensor_input_callback,10)
        self.sensor_input_sub  # prevent unused variable warning
        self.goal_input_sub = self.create_subscription(PoseStamped,'/goal_pose',self.goal_input_callback,10)
        self.goal_input_sub  # prevent unused variable warning

    # SUBSCRIBER CALLBACK

    def sensor_input_callback(self, msg):
        self.x = msg.pose.position.x
        self.y = msg.pose.position.y
        quaternion = [
            msg.pose.orientation.x,
            msg.pose.orientation.y,
            msg.pose.orientation.z,
            msg.pose.orientation.w
            ]
        _,_,self.phi = euler_from_quaternion(quaternion)

        phid = np.arctan2(self.yd - self.y,self.xd - self.x)
        error = phid - self.phi
        if self.correction:
            error = np.arctan2(np.sin(error),np.cos(error))

        self.cmd_vel.angular.z = self.gain*error
        self.publisher_cmd_vel.publish(self.cmd_vel)

        self.get_logger().info('action: "%s"' % error)

    def goal_input_callback(self, msg):
        self.xd = msg.pose.position.x
        self.yd = msg.pose.position.y 

    # INIT GOAL POSE

    def init_goal_pose(self):
        goal_pose = PoseStamped()
        goal_pose.header.stamp = self.get_clock().now().to_msg()
        goal_pose.header.frame_id = 'map'
        goal_pose.pose.position.x = self.xd
        goal_pose.pose.position.y = self.yd
        self.publisher_goal_pose.publish(goal_pose)


def main(args=None):
    rclpy.init(args=args)

    node = Controlador()
    node.init_goal_pose()
    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()