# Copyright 2023 Davinson Castano Cano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy

import rclpy
from rclpy.node import Node

from geometry_msgs.msg import Twist, PoseStamped, PoseWithCovarianceStamped
from nav_msgs.msg import Odometry
from tf_transformations import quaternion_from_euler, euler_from_quaternion

class Uniciclo(Node):

    def __init__(self):
        super().__init__('uniciclo')
        
        self.v = 0.0
        self.w = 0.0

        self.x = 0.0
        self.y = 0.0
        self.phi = 0.0
        
        self.declare_parameter('dt',1/20)    # seconds
        self.dt = self.get_parameter('dt').value

        self.outPose = PoseStamped()
        self.outOdom = Odometry()

        # TOPIC PUBLISHERS

        self.publisher_pose = self.create_publisher(PoseStamped, '/pose', 1)
        self.publisher_odom = self.create_publisher(Odometry, '/odom', 1)
        self.timer = self.create_timer(self.dt, self.timer_callback)

        # TOPIC SUBSCRIBERS

        self.input_sub = self.create_subscription(Twist,'/cmd_vel',self.input_callback,10)
        self.input_sub  # prevent unused variable warning
        self.reset_sub = self.create_subscription(PoseWithCovarianceStamped,'/initialpose',self.reset_callback,10)
        self.reset_sub  # prevent unused variable warning

    # PUBLISHER CALLBACK

    def timer_callback(self):
        
        self.phi += self.dt*(self.w)
        self.x += self.dt*(self.v*numpy.cos(self.phi))
        self.y += self.dt*(self.v*numpy.sin(self.phi))

        self.outPose.header.stamp = self.get_clock().now().to_msg()
        self.outPose.header.frame_id = 'map'
        self.outPose.pose.position.x = self.x
        self.outPose.pose.position.y = self.y
        q = quaternion_from_euler(0, 0, self.phi)
        self.outPose.pose.orientation.x = q[0]
        self.outPose.pose.orientation.y = q[1]
        self.outPose.pose.orientation.z = q[2]
        self.outPose.pose.orientation.w = q[3]
        
        self.publisher_pose.publish(self.outPose)

        self.outOdom.header = self.outPose.header
        self.outOdom.pose.pose.position = self.outPose.pose.position
        self.outOdom.pose.pose.orientation = self.outPose.pose.orientation
        self.outOdom.twist.twist.linear.x = self.v
        self.outOdom.twist.twist.angular.z = self.w

        self.publisher_odom.publish(self.outOdom)

    # SUBSCRIBER CALLBACK

    def input_callback(self, msg):
        self.v = float(msg.linear.x)
        self.w = float(msg.angular.z)

    def reset_callback(self, msg):
        self.x = msg.pose.pose.position.x
        self.y = msg.pose.pose.position.y
        quaternion = [
            msg.pose.pose.orientation.x,
            msg.pose.pose.orientation.y,
            msg.pose.pose.orientation.z,
            msg.pose.pose.orientation.w
            ]
        _,_,self.phi = euler_from_quaternion(quaternion)


def main(args=None):
    rclpy.init(args=args)

    node = Uniciclo()
    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()