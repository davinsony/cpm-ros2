from setuptools import setup

package_name = 'plataformas_moviles'

setup(
    name=package_name,
    version='2023.1.30',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Davinson Castaño',
    maintainer_email='cpm@davinsony.com',
    description='Clase 3, plataformas moviles',
    license='GPL-3.0-only',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'uniciclo = plataformas_moviles.uniciclo:main',
            'controlador = plataformas_moviles.controlador:main',
        ],
    },
)