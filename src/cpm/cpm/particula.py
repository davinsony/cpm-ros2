import numpy
import rclpy
from rclpy.node import Node
from tf2_ros import TransformBroadcaster
from tf_transformations import quaternion_from_euler
from geometry_msgs.msg import PointStamped,WrenchStamped,TransformStamped

class Nodo(Node):

    def __init__(self):
        super().__init__('robot')

        self.declare_parameter('x0',0)
        x0 = self.get_parameter('x0').value
        self.declare_parameter('y0',0)
        y0 = self.get_parameter('y0').value
        self.declare_parameter('z0',0)
        z0 = self.get_parameter('z0').value

        self.A = numpy.array([
            [0,1,0,0,0,0],
            [0,0,0,0,0,0],
            [0,0,0,1,0,0],
            [0,0,0,0,0,0],
            [0,0,0,0,0,1],
            [0,0,0,0,0,0]
        ])
        self.B = numpy.array([
            [0,0,0],
            [1,0,0],
            [0,0,0],
            [0,1,0],
            [0,0,0],
            [0,0,1],
        ])
        self.C = numpy.array([
            [1,0,0,0,0,0],
            [0,0,1,0,0,0],
            [0,0,0,0,1,0]
        ])
        self.x = numpy.array([[x0],[0],[y0],[0],[z0],[0]],dtype='float64')
        self.y = numpy.zeros([3,1])
        self.u = numpy.zeros([3,1])

        self.declare_parameter('dt',0.05)    # seconds
        self.dt = self.get_parameter('dt').value

        self.point_pub  = self.create_publisher(PointStamped, 'position', 10)
        self.timer = self.create_timer(self.dt, self.timer_callback)

        self.input_sub = self.create_subscription(
            WrenchStamped,
            'forces',
            self.input_callback,
            10)
        self.input_sub  # prevent unused variable warning

        self.br = TransformBroadcaster(self)

    def input_callback(self, msg):

        self.u = numpy.array([
            [msg.wrench.force.x],
            [msg.wrench.force.y],
            [msg.wrench.force.z]
        ])

    def timer_callback(self):
        
        self.x += self.dt*(numpy.matmul(self.A,self.x)+numpy.matmul(self.B,self.u))
        self.y = numpy.matmul(self.C,self.x)

        msg = PointStamped()
        msg.header.stamp = self.get_clock().now().to_msg()
        msg.header.frame_id = 'world'
        msg.point.x = self.y.item(0)
        msg.point.y = self.y.item(1)
        msg.point.z = self.y.item(2)
        self.point_pub.publish(msg)
        self.send_transform(msg)

    def send_transform(self, msg):
        t = TransformStamped()

        # Read message content and assign it to
        # corresponding tf variables
        t.header.stamp = self.get_clock().now().to_msg()
        t.header.frame_id = 'world'
        t.child_frame_id = 'particle'

        # Turtle only exists in 2D, thus we get x and y translation
        # coordinates from the message and set the z coordinate to 0
        t.transform.translation.x = msg.point.x
        t.transform.translation.y = msg.point.y
        t.transform.translation.z = msg.point.z

        # For the same reason, turtle can only rotate around one axis
        # and this why we set rotation in x and y to 0 and obtain
        # rotation in z axis from the message
        q = quaternion_from_euler(0, 0, 0)
        t.transform.rotation.x = q[0]
        t.transform.rotation.y = q[1]
        t.transform.rotation.z = q[2]
        t.transform.rotation.w = q[3]

        # Send the transformation
        self.br.sendTransform(t)

def main(args=None):
    
    rclpy.init(args=args)
    nodo = Nodo()
    rclpy.spin(nodo)
    nodo.get_logger().info('Closing')
    nodo.destroy_node() # Destroy the node explicitly (optional - otherwise it will be done automatically when the garbage collector destroys the node object)
    rclpy.shutdown()


if __name__ == '__main__':
    main()