import numpy
import rclpy
from rclpy.node import Node
from tf2_ros import TransformBroadcaster
from tf_transformations import quaternion_from_euler
from geometry_msgs.msg import PointStamped,WrenchStamped,TransformStamped
from datetime import datetime as time

class Nodo(Node):

    def __init__(self):
        super().__init__('robot')

        self.declare_parameter('k1',1)
        k1 = self.get_parameter('k1').value
        self.declare_parameter('k2',2)
        k2 = self.get_parameter('k2').value

        self.K = numpy.array([
            [k1,k2,0,0,0,0],
            [0,0,k1,k2,0,0],
            [0,0,0,0,k1,k2]
        ])
        self.y = numpy.zeros([6,1])
        self.r = numpy.zeros([6,1])
        self.u = numpy.zeros([3,1])

        self.y_last = numpy.zeros([6,1])
        self.t_last = 0

        self.force_pub  = self.create_publisher(WrenchStamped, 'forces', 10)

        self.input_sub = self.create_subscription(
            PointStamped,
            'position',
            self.input_callback,
            10)
        self.input_sub  # prevent unused variable warning

        self.goal_sub = self.create_subscription(
            PointStamped,
            'clicked_point',
            self.set_reference,
            10)
        self.goal_sub  # prevent unused variable warning

        self.br = TransformBroadcaster(self)

    def set_reference(self, msg):

        self.r = numpy.array([
            [msg.point.x],[0],
            [msg.point.y],[0],
            [msg.point.z],[0]
        ])

    def input_callback(self, msg):

        self.y = numpy.array([
            [msg.point.x],[0],
            [msg.point.y],[0],
            [msg.point.z],[0]
        ])

        if self.t_last!=0:
            t_now = time.now()
            delta_t = t_now - self.t_last
            delta_t = delta_t.total_seconds()
            delta_y = self.y - self.y_last

            self.y[1,0] = delta_y[0,0]/delta_t
            self.y[3,0] = delta_y[2,0]/delta_t
            self.y[5,0] = delta_y[4,0]/delta_t

            self.y_last = self.y
            self.t_last = t_now

        else:
            self.t_last = time.now()

        error = self.y - self.r

        self.u = - numpy.matmul(self.K,error)

        msg = WrenchStamped()
        msg.header.stamp = self.get_clock().now().to_msg()
        msg.header.frame_id = 'particle'
        msg.wrench.force.x = self.u.item(0)
        msg.wrench.force.y = self.u.item(1)
        msg.wrench.force.z = self.u.item(2)
        self.force_pub.publish(msg)

    def timer_callback(self):
        
        self.x += self.dt*(numpy.matmul(self.A,self.x)+numpy.matmul(self.B,self.u))
        self.y = numpy.matmul(self.C,self.x)

        msg = PointStamped()
        msg.header.stamp = self.get_clock().now().to_msg()
        msg.header.frame_id = 'world'
        msg.point.x = self.y.item(0)
        msg.point.y = self.y.item(1)
        msg.point.z = self.y.item(2)
        self.force_pub.publish(msg)
        self.send_transform(msg)

    def send_transform(self, msg):
        t = TransformStamped()

        # Read message content and assign it to
        # corresponding tf variables
        t.header.stamp = self.get_clock().now().to_msg()
        t.header.frame_id = 'world'
        t.child_frame_id = 'point'

        # Turtle only exists in 2D, thus we get x and y translation
        # coordinates from the message and set the z coordinate to 0
        t.transform.translation.x = msg.point.x
        t.transform.translation.y = msg.point.y
        t.transform.translation.z = msg.point.z

        # For the same reason, turtle can only rotate around one axis
        # and this why we set rotation in x and y to 0 and obtain
        # rotation in z axis from the message
        q = quaternion_from_euler(0, 0, 0)
        t.transform.rotation.x = q[0]
        t.transform.rotation.y = q[1]
        t.transform.rotation.z = q[2]
        t.transform.rotation.w = q[3]

        # Send the transformation
        self.br.sendTransform(t)

def main(args=None):
    
    rclpy.init(args=args)
    nodo = Nodo()
    rclpy.spin(nodo)
    nodo.get_logger().info('Closing')
    nodo.destroy_node() # Destroy the node explicitly (optional - otherwise it will be done automatically when the garbage collector destroys the node object)
    rclpy.shutdown()


if __name__ == '__main__':
    main()