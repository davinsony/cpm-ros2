from setuptools import setup

package_name = 'cpm'

setup(
    name=package_name,
    version='2022.9.7',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Davinson Castaño',
    maintainer_email='cpm@davinsony.com',
    description='Ejectutables para el curso CPM',
    license='GPL-3.0-only',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'particula = cpm.particula:main',
            'particula_controlador = cpm.particula_controlador:main',
            'particula_derivada = cpm.particula_solucion_derivacion:main',
            'particula_luenberger = cpm.particula_solucion_Luenberger:main',
        ],
    },
)
