from setuptools import setup

package_name = 'intro_control'

setup(
    name=package_name,
    version='2022.7.14',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='CPM',
    maintainer_email='cpm@davinsony.com',
    description='Modelo del carro y diferentes controladores',
    license='Apache License 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'timer = intro_control.timer:main',
            'car = intro_control.car:main',
            'bang_bang = intro_control.bang_bang:main',
            'pid = intro_control.pid:main',
        ],
    },
)
