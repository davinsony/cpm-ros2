# Copyright 2022 Davinson Castano Cano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rclpy
from rclpy.node import Node

from std_msgs.msg import Float32

class Dinamics(Node):

    def __init__(self):
        super().__init__('timer')
        self.publisher_ = self.create_publisher(Float32, 'time', 10)
        
        self.declare_parameter('dt',1.0)    # seconds
        self.dt = self.get_parameter('dt').value
        
        self.timer = self.create_timer(self.dt, self.timer_callback)
        
        self.x = 0.0

    def timer_callback(self):
        
        self.x += self.dt

        msg = Float32()
        msg.data = self.x
        
        self.publisher_.publish(msg)
        self.get_logger().info('Publishing: "%s"' % msg.data)

def main(args=None):
    rclpy.init(args=args)

    timer_publisher = Dinamics()

    rclpy.spin(timer_publisher)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    timer_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()