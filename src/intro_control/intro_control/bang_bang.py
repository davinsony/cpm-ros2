# Copyright 2022 Davinson Castano Cano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rclpy
from rclpy.node import Node

from std_msgs.msg import Float32

class Controller(Node):

    def __init__(self):
        super().__init__('controller')
        
        self.y = 0.0
        self.u = 0.0
        self.e = 0.0
        self.ref = 0.0

        self.declare_parameter('umax',100.0)      # coeficiente de transmision electro-mecanica
        self.umax = self.get_parameter('umax').value
        
        self.declare_parameter('dt',1/15)    # seconds
        self.dt = self.get_parameter('dt').value

        self.publisher_ = self.create_publisher(Float32, 'input', 10)
        self.timer = self.create_timer(self.dt, self.timer_callback)

        self.speed_sub = self.create_subscription(
            Float32,
            'speed',
            self.speed_callback,
            10)
        self.speed_sub  # prevent unused variable warning

        self.reference_sub = self.create_subscription(
            Float32,
            'reference',
            self.reference_callback,
            10)
        self.reference_sub  # prevent unused variable warning

    def speed_callback(self, msg):
        self.y = msg.data

    def reference_callback(self, msg):
        self.ref = msg.data

    def timer_callback(self):
        
        self.e = self.ref - self.y

        if self.e > 0:
            self.u = self.umax
        elif self.e < 0:
            self.u = -self.umax
        else:
            self.u = 0.0

        msg = Float32()
        msg.data = self.u
        
        self.publisher_.publish(msg)
        self.get_logger().info('action: "%s"' % msg.data)

def main(args=None):
    rclpy.init(args=args)

    controller = Controller()

    rclpy.spin(controller)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    controller.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()