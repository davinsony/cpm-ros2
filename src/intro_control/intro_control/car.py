# Copyright 2022 Davinson Castano Cano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rclpy
from rclpy.node import Node

from std_msgs.msg import Float32

class Dinamics(Node):

    def __init__(self):
        super().__init__('car')
        
        self.x = 0.0
        self.u = 0.0

        self.declare_parameter('dt',1/15)    # seconds
        self.dt = self.get_parameter('dt').value

        self.declare_parameter('c',150.0)      # coeficiente de transmision electro-mecanica
        self.c = self.get_parameter('c').value

        self.declare_parameter('m',500.0)      # masa del vehiculo
        self.m = self.get_parameter('m').value

        self.declare_parameter('gamma',1/4)  # coeficiente de friccion 
        self.gamma = self.get_parameter('gamma').value
        
        self.publisher_ = self.create_publisher(Float32, 'speed', 10)
        self.timer = self.create_timer(self.dt, self.timer_callback)

        self.subscription = self.create_subscription(
            Float32,
            'input',
            self.listener_callback,
            10)
        self.subscription  # prevent unused variable warning

    def listener_callback(self, msg):
        self.u = msg.data
        #self.get_logger().info('Input: "%s"' % msg.data)

    def timer_callback(self):
        
        self.x += self.dt*((self.c/self.m)*self.u - self.gamma*self.x)

        msg = Float32()
        msg.data = self.x
        
        self.publisher_.publish(msg)
        self.get_logger().info('speed: "%s"' % msg.data)

def main(args=None):
    rclpy.init(args=args)

    car_publisher = Dinamics()

    rclpy.spin(car_publisher)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    car_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()