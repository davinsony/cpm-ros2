# Copyright 2022 Davinson Castano Cano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rclpy
from rclpy.node import Node
from rclpy.parameter import Parameter
from rcl_interfaces.msg import SetParametersResult

from std_msgs.msg import Float32

class Controller(Node):

    def __init__(self):
        super().__init__('controller')
        
        self.y = 0.0
        self.u = 0.0
        self.e = 0.0
        self.e_last = 0.0
        self.e_sum  = 0.0
        self.ref = 0.0

        self.declare_parameter('kp',1.0)      # coeficiente control P
        self.kp = self.get_parameter('kp').value

        self.declare_parameter('ki',0.0)      # coeficiente control I
        self.ki = self.get_parameter('ki').value

        self.declare_parameter('kd',0.0)      # coeficiente control D
        self.kd = self.get_parameter('kd').value
        
        self.declare_parameter('dt',1/15)    # seconds
        self.dt = self.get_parameter('dt').value

        self.publisher_ = self.create_publisher(Float32, 'input', 10)
        self.timer = self.create_timer(self.dt, self.timer_callback)

        self.speed_sub = self.create_subscription(
            Float32,
            'speed',
            self.speed_callback,
            10)
        self.speed_sub  # prevent unused variable warning

        self.reference_sub = self.create_subscription(
            Float32,
            'reference',
            self.reference_callback,
            10)
        self.reference_sub  # prevent unused variable warning

        self.add_on_set_parameters_callback(self.parameter_callback)

    def parameter_callback(self, params):
        for param in params:
            if param.name == 'kp' and param.type_ == Parameter.Type.DOUBLE:
                self.kp = param.value
            if param.name == 'ki' and param.type_ == Parameter.Type.DOUBLE:
                self.ki = param.value
            if param.name == 'kd' and param.type_ == Parameter.Type.DOUBLE:
                self.kd = param.value
        return SetParametersResult(successful=True)

    def speed_callback(self, msg):
        self.y = msg.data

    def reference_callback(self, msg):
        self.ref = msg.data

    def timer_callback(self):
        
        self.e  = self.ref - self.y
        error_d = (self.e - self.e_last)/self.dt
        self.e_sum += self.dt*self.e

        self.u = self.kp*self.e + self.ki*self.e_sum + self.kd*error_d

        self.e_last = self.e

        msg = Float32()
        msg.data = self.u
        
        self.publisher_.publish(msg)
        self.get_logger().info('action: "%s"' % msg.data)

def main(args=None):
    rclpy.init(args=args)

    controller = Controller()

    rclpy.spin(controller)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    controller.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()