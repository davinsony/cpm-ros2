# Introducción al control 

Para está clase contamos con 4 ejectutables:

- timer : nos muestra la dinámica sencilla de un reloj con un dt, definido por el parámetro dt
- car : simula la dinámica de un carro, tiene como salida la velocidad _speed_ del carro y tiene como entrada el porcentaje de aceleración del acelerador que también funciona como freno (desaceleración). Sigue la dinámica:

    $$\dot{x} = \frac{c}{m}u+\gamma\,x$$

- bang_bang : es un controlador que acelera al máximo o frena al máximo para mantener una velocidad de referencia.
- pid : es la implementación del controlador PID. 

## Ejemplo 1 : Timer

1. Hacer source del `setup.bash` estando en la carpeta `cpm-ros2`
    
    ```
    source install/setup.bash
    ```

2. Correr el nodo `timer`

    ```
    ros2 run intro_control timer
    ros2 run intro_control timer --ros-args -p dt:=0.05
    ```

    Si deseamos cambiar el parametro dt del nodo timer usar la segunda linea. 

3. Visualizar la variable `time` en rqt_plot

    ```
    ros2 run rqt_plot rqt_plot
    ```

## Ejemplo 2 : Carro con control bang-bang

Despues de hacer el source de los recursos (`setup.bash`). Iniciamos corriendo el nodo `car`
```
ros2 run intro_control car
```
para usar los controladores de la velocidad del vehículo podemos correr uno de ellos, en un segundo terminal.
```
ros2 run intro_control bang_bang
```
en un tercer terminal corremos, este nos sirve para ver la variables importante del sistema
```
ros2 run rqt_plot rqt_plot /speed/data /input/data /reference/data
```
y en un cuarto terminal cambiaremos la velocidad de referencia (_reference_)
```
ros2 topic pub /reference std_msgs/msg/Float32 {data: 70.0}
```

## Ejemplo 3 : Carro con control P

A diferencia del ejemplo 2 no usaremos el control `bang_bang` sino `pid`, en el segundo terminal corremos:
```
ros2 run intro_control pid
```
Podremos cambiar los parametros del controlador via 
```
ros2 param set /controller kp 10.0
```