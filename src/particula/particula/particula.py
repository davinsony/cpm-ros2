# Copyright 2022 Davinson Castano Cano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy
import random

import rclpy
from rclpy.node import Node

from turtlesim.srv import Kill, Spawn
from turtlesim.msg import Pose
from geometry_msgs.msg import Twist, Wrench

class Particle(Node):

    def __init__(self):
        super().__init__('particula')
        
        self.A = numpy.array([
            [0,0],
            [0,0],
        ])
        self.B = numpy.array([
            [1,0],
            [0,1],
        ])
        self.C = numpy.array([
            [1,0],
            [0,1],
        ])

        self.x = numpy.zeros([2,1])
        self.y = numpy.zeros([2,1])
        self.u = numpy.zeros([2,1])
        
        self.declare_parameter('dt',1/20)    # seconds
        self.dt = self.get_parameter('dt').value

        self.declare_parameter('rebote',0.9)    # seconds
        self.rebote = self.get_parameter('rebote').value

        self.msg = Twist()

        # TOPIC PUBLISHERS

        self.publisher_ = self.create_publisher(Twist, 'particula/cmd_vel', 1)
        self.timer = self.create_timer(self.dt, self.timer_callback)

        # TOPIC SUBSCRIBERS

        self.input_sub = self.create_subscription(Wrench,'particula/force',self.input_callback,10)
        self.input_sub  # prevent unused variable warning

        self.pose_sub = self.create_subscription(Pose,'particula/pose',self.pose_callback,10)
        self.pose_sub  # prevent unused variable warning

        # SERVICE CLIENTS
        
        self.client_kill = self.create_client(Kill, 'kill')
        while not self.client_kill.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('service not available, waiting again...')
        self.kill = Kill.Request()

        self.client_spawn = self.create_client(Spawn, 'spawn')
        while not self.client_spawn.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('service not available, waiting again...')
        self.spawn = Spawn.Request()

    # PUBLISHER CALLBACK

    def timer_callback(self):
        
        self.x += self.dt*(numpy.matmul(self.A,self.x)+numpy.matmul(self.B,self.u))
        self.y = numpy.matmul(self.C,self.x)

        self.msg.linear.x = self.y.item(0)
        self.msg.linear.y = self.y.item(1)
        
        self.publisher_.publish(self.msg)

    # SUBSCRIBER CALLBACK

    def input_callback(self, msg):
        self.u = numpy.array([[msg.force.x],[msg.force.y]])

    def pose_callback(self, msg):
        pos0 = float(msg.x)
        pos1 = float(msg.y)
        if pos0 >= 11 or pos0 <= 0:
            self.x[0,0] *= -self.rebote
        if pos1 >= 11 or pos1 <= 0:
            self.x[1,0] *= -self.rebote

    # SERVICE REQUEST

    def kill_request(self,name):
        self.kill.name = name
        self.future = self.client_kill.call_async(self.kill)
        rclpy.spin_until_future_complete(self, self.future)
        return self.future.result()
    def spawn_request(self,x,y,theta,name):
        self.spawn.x = x
        self.spawn.y = y
        self.spawn.theta = theta
        self.spawn.name = name
        self.future = self.client_spawn.call_async(self.spawn)
        rclpy.spin_until_future_complete(self, self.future)
        return self.future.result()

def main(args=None):
    rclpy.init(args=args)

    node = Particle()
    response = node.kill_request("turtle1")
    px = random.uniform(1,10)
    py = random.uniform(1,10)
    response = node.spawn_request(px,py,0.0,"particula")

    rclpy.spin(node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()