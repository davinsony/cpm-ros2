from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='turtlesim',
            executable='turtlesim_node',
            name='window',
            parameters=[
                {'background_r':32},
                {'background_g':32},
                {'background_b':32},
            ]
        ),
        Node(
            package='particula',
            executable='particula',
            name='particula'
        )
    ])