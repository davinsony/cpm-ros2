# CPM Ejercicios ROS2

Ejercicios para el curso de Control de Productos Mecatrónicos

Para poder usar estos ejercicios en tu computador debes instalar `git` y `colcon`, en un terminal usa el siguiente comando:
```
sudo apt install git
sudo apt install python3-colcon-common-extensions
```
una vez tenemos git, podemos clonar este repositorio en nuestra máquina con ROS. 

```
git clone https://gitlab.com/davinsony/cpm-ros2.git
```

Para la versión de ROS en la máquina virtual debemos definir el camino para acceder a los recursos de ROS.
```
echo "source /opt/ros/$ROS_DISTRO/setup.bash" >> ~/.bashrc
```
cambia `$ROS_DISTRO` por el nombre de tu distribución de ROS 2 en caso de ser diferente.

## Para empezar

Para poder usar este repositorio y correr los nodos en ROS2, deberemos compilar los paquetes del repositorio usando, debemos estar en la carpeta `cpm-ros2`:

```bash
colcon build
```

Una vez compilados, vamos agregar los paquetes a las variables de entorno usando:

```bash
source install/setup.bash
```

Ahora si podremos correr nuestro código usando, por ejemplo:

```bash
ros2 run cpm particula
```